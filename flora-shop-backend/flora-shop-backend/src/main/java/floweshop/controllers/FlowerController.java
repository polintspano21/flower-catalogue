package floweshop.controllers;

import floweshop.models.Flower;
import floweshop.models.dtos.CreateFlowerRequest;
import floweshop.models.dtos.FlowerFilters;
import floweshop.models.dtos.Response;
import floweshop.services.FlowerService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class FlowerController {

    private FlowerService flowerService;

    @PostMapping("/flower/add")
    public ResponseEntity<Response<Flower>> addFlower(@RequestBody CreateFlowerRequest createFlowerRequest) {
        return flowerService.addFlower(createFlowerRequest);
    }

    @PostMapping("/flowers/get")
    public ResponseEntity<Response<Iterable<Flower>>> getFlowers(@RequestBody FlowerFilters flowerFilters) {
        return flowerService.getFlowers(flowerFilters);
    }

    @GetMapping("/flower/get")
    public ResponseEntity<Response<Flower>> getFlower(@RequestParam("flowerId") Long id) {
        return flowerService.getFlower(id);
    }

    @DeleteMapping("/flower/delete")
    public ResponseEntity<Response<Flower>> deleteFlower(@RequestParam("flowerId") Long id) {
        return flowerService.deleteFlower(id);
    }

    @PutMapping("/flower/update")
    public ResponseEntity<Response<Flower>> updateFlower(@RequestBody Flower flower) {
        return flowerService.updateFlower(flower);
    }
}
