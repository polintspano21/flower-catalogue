package floweshop.controllers;

import floweshop.models.User;
import floweshop.models.dtos.RegisterUserDto;
import floweshop.models.dtos.Response;
import floweshop.models.dtos.UserLoginDto;
import floweshop.services.AuthenticationService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class AuthenticationController {

    private AuthenticationService authenticationService;

    @PostMapping("/user/register")
    public ResponseEntity<Response<User>> registerUser(@RequestBody RegisterUserDto registerUserDto) {
        return authenticationService.registerUser(registerUserDto);
    }

    @PostMapping("/user/login")
    public ResponseEntity<Response<User>> loginUser(@RequestBody UserLoginDto user) {
        return authenticationService.loginUser(user);
    }
}
