package floweshop.controllers;

import floweshop.models.Order;
import floweshop.models.dtos.GetOrdersRequest;
import floweshop.models.dtos.Response;
import floweshop.services.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class OrderController {

    private OrderService orderService;

    @PostMapping("/order/add")
    public ResponseEntity<Response<Order>> addOrder(@RequestBody Order order) {
        return orderService.addOrder(order);
    }

    @GetMapping("/orders/get")
    public ResponseEntity<Response<List<GetOrdersRequest>>> getOrders() {
        return orderService.getOrders();
    }
}
