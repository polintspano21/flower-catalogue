package floweshop.models;

import floweshop.models.dtos.CreateFlowerRequest;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import floweshop.models.enums.FlowerCategory;

import javax.naming.Name;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "flowers")
public class Flower {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "price", nullable = false)
    private int price;

    @Enumerated(EnumType.STRING)
    @Column(name = "category", nullable = false)
    private FlowerCategory category;

    public Flower(CreateFlowerRequest createFlowerRequest) {
        name = createFlowerRequest.getName();
        price = createFlowerRequest.getPrice();
        category = createFlowerRequest.getCategory();
        description = createFlowerRequest.getDescription();
    }
}
