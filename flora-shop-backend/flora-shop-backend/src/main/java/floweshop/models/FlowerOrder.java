package floweshop.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "flower_order_entities")
public class FlowerOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "flowerId", nullable = false)
    private Long flowerId;

    @Column(name = "quantity", nullable = false)
    private int quantity;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    private Order order;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlowerOrder )) return false;
        return id != null && id.equals(((FlowerOrder) o).getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
