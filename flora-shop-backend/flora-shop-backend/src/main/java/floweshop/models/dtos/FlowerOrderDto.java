package floweshop.models.dtos;

import floweshop.models.Flower;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FlowerOrderDto {
    private Flower flower;
    private int quantity;
}
