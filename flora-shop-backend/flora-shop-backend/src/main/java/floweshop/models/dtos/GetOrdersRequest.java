package floweshop.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GetOrdersRequest {
    private String address;

    private List<FlowerOrderDto> flowerOrders;
}
