package floweshop.models.dtos;

import floweshop.models.enums.FlowerCategory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FlowerFilters {

    private String name;
    private Integer minPrice;
    private Integer maxPrice;
    private List<FlowerCategory> categories;
}
