package floweshop.models.dtos;

import floweshop.models.enums.FlowerCategory;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CreateFlowerRequest {
    private String name;
    private int price;

    private String description;
    private String imageUrl;

    @Enumerated(EnumType.STRING)
    private FlowerCategory category;
}
