package floweshop.models.enums;

public enum UserRole {
    ADMIN,
    USER
}
