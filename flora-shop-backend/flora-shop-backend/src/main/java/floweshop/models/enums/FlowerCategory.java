package floweshop.models.enums;

public enum FlowerCategory {
    INDOOR,
    OUTDOOR,
    EXOTIC
}
