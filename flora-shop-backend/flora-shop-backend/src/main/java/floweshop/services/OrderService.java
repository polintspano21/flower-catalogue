package floweshop.services;

import floweshop.models.Order;
import floweshop.models.dtos.FlowerOrderDto;
import floweshop.models.dtos.GetOrdersRequest;
import floweshop.models.dtos.Response;
import floweshop.repositories.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class OrderService {

    private OrderRepository orderRepository;
    private FlowerService flowerService;

    public ResponseEntity<Response<Order>> addOrder(Order order) {
        return new ResponseEntity<>(new Response<>("Order successfully created!", orderRepository.save(order)), HttpStatus.OK);
    }

    public ResponseEntity<Response<List<GetOrdersRequest>>> getOrders() {
        List<GetOrdersRequest> ordersRequest = new ArrayList<>();

        orderRepository.findAll().forEach((order) -> {
            List<FlowerOrderDto> flowerOrders = order.getFlowerOrders().stream().map((flowerOrder) -> new FlowerOrderDto(flowerService.getFlowerById(flowerOrder.getFlowerId()), flowerOrder.getQuantity())).toList();

            ordersRequest.add(new GetOrdersRequest(order.getAddress(), flowerOrders));
        });

        return new ResponseEntity<>(new Response<>("All orders were retrieved!", ordersRequest), HttpStatus.OK);
    }
}
