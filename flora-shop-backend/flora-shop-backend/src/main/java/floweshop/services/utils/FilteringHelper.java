package floweshop.services.utils;

import floweshop.models.Flower;
import floweshop.models.dtos.FlowerFilters;
import org.springframework.stereotype.Component;
import java.util.function.BiPredicate;
import java.util.stream.StreamSupport;

@Component
public class FilteringHelper {

    public BiPredicate<FlowerFilters, Flower> filterValue() {
        BiPredicate<FlowerFilters, Flower> minPricePr =
                (pFilters, pFlower) -> pFilters.getMinPrice() == null || pFilters.getMinPrice() <= pFlower.getPrice();

        BiPredicate<FlowerFilters, Flower> maxPricePr =
                (pFilters, pFlower) -> pFilters.getMaxPrice() == null || pFilters.getMaxPrice() >= pFlower.getPrice();

        BiPredicate<FlowerFilters, Flower> containsName =
                (pFilters, pFlower) -> pFilters.getName() == null || pFlower.getName().contains(pFilters.getName());

        BiPredicate<FlowerFilters, Flower> hasCategory =
                (pFilters, pFlower) ->
                        pFilters.getCategories() == null ||
                                pFilters.getCategories().stream()
                                        .anyMatch((category) -> category.equals(pFlower.getCategory()));

        return minPricePr.and(maxPricePr).and(containsName).and(hasCategory);
    }
}
