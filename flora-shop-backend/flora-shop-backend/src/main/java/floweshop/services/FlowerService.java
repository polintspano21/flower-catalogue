package floweshop.services;

import floweshop.models.Flower;
import floweshop.models.dtos.CreateFlowerRequest;
import floweshop.models.dtos.FlowerFilters;
import floweshop.models.dtos.Response;
import floweshop.repositories.FlowerRepository;
import floweshop.services.utils.FilteringHelper;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class FlowerService {

    private FlowerRepository flowerRepository;
    private FilteringHelper filteringHelper;

    public ResponseEntity<Response<Flower>> addFlower(CreateFlowerRequest createFlowerRequest) {
        return new ResponseEntity<>(
                new Response<>("Flower created successfully!", flowerRepository.save(new Flower(createFlowerRequest))),
                HttpStatus.OK
        );
    }

    public ResponseEntity<Response<Iterable<Flower>>> getFlowers(FlowerFilters flowerFilters) {
        Iterable<Flower> filteredFlowers = StreamSupport.stream(flowerRepository.findAll().spliterator(), false).filter((flower) -> filteringHelper.filterValue().test(flowerFilters, flower)).collect(Collectors.toList());

        return new ResponseEntity<>(new Response<>("Flowers successfully retrieved!", filteredFlowers), HttpStatus.OK);
    }

    public ResponseEntity<Response<Flower>> getFlower(Long id) {
        return new ResponseEntity<>(new Response<>("Flower successfully retrieved!", flowerRepository.findById(id).get()), HttpStatus.OK);
    }

    public List<Flower> getOrderFlowers(Iterable<Long> ids) {
        return StreamSupport.stream(flowerRepository.findAllById(ids).spliterator(), false).toList();
    }

    public Flower getFlowerById(Long id) {
        return flowerRepository.findById(id).get();
    }

    public ResponseEntity<Response<Flower>> updateFlower(Flower flower) {
        return new ResponseEntity<>(new Response<>("Flower updated successfully!", flowerRepository.save(flower)), HttpStatus.OK);
    }

    public ResponseEntity<Response<Flower>> deleteFlower(Long id) {
        flowerRepository.deleteById(id);

        return new ResponseEntity<>(new Response<>("Flower has been deleted successfully!", null), HttpStatus.OK);
    }
}
