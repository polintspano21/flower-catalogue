package floweshop.services;

import floweshop.models.User;
import floweshop.models.dtos.RegisterUserDto;
import floweshop.models.dtos.Response;
import floweshop.models.dtos.UserLoginDto;
import floweshop.models.enums.UserRole;
import floweshop.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AuthenticationService {

    private UserRepository userRepository;

    public ResponseEntity<Response<User>> registerUser(RegisterUserDto registerUserDto) {
        if (userRepository.existsUserByUsername(registerUserDto.getUsername())) {
            return new ResponseEntity<>(new Response<>("User already exists!", null), HttpStatus.BAD_REQUEST);
        }

        User userToRegister = new User();
        userToRegister.setPassword(registerUserDto.getPassword());
        userToRegister.setUsername(registerUserDto.getUsername());
        userToRegister.setUserRole(UserRole.USER);

        return new ResponseEntity<>(new Response<>("User successfully registered!", userRepository.save(userToRegister)), HttpStatus.OK);
    }

    public ResponseEntity<Response<User>> loginUser(UserLoginDto user) {
        Optional<User> userToLogin = userRepository.findUserByUsername(user.getUsername());

        if (userToLogin.isPresent()) {
            if (Objects.equals(userToLogin.get().getPassword(), user.getPassword())) {
                return new ResponseEntity<>(new Response<>("Successfully logged in!", userToLogin.get()), HttpStatus.OK);
            }

            return new ResponseEntity<>(new Response<>("Wrong password or username!", null), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new Response<>("Wrong password or username!", null), HttpStatus.BAD_REQUEST);
    }
}
