package floweshop.repositories;

import floweshop.models.Flower;
import floweshop.models.enums.FlowerCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlowerRepository extends CrudRepository<Flower, Long> { }
