package floweshop.repositories;

import floweshop.models.FlowerOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlowerOrderRepository extends CrudRepository<FlowerOrder, Long> {
    Iterable<FlowerOrder> findFlowerOrderByFlowerId(Long flowerId);
}