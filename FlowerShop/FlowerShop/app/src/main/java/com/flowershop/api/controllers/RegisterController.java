package com.flowershop.api.controllers;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.flowershop.LoginActivity;
import com.flowershop.api.FloraApi;
import com.flowershop.api.models.AuthenticationDto;
import com.flowershop.api.models.AuthenticationRequestResponse;
import com.flowershop.api.models.RequestResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterController implements Callback<RequestResponse<AuthenticationRequestResponse>> {
    private final String BASE_URL = "http://192.168.0.107:8080";
    private final Context context;

    public RegisterController(Context context) {
        this.context = context;
    }

    public void register(AuthenticationDto authenticationDto) {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        FloraApi gerritAPI = retrofit.create(FloraApi.class);

        Call<RequestResponse<AuthenticationRequestResponse>> call = gerritAPI.registerUser(authenticationDto);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<RequestResponse<AuthenticationRequestResponse>> call, Response<RequestResponse<AuthenticationRequestResponse>> response) {
        if (response.code() == 200) {
            context.startActivity(new Intent(context, LoginActivity.class));
        } else {
            Toast.makeText(context, "Username already exists!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call<RequestResponse<AuthenticationRequestResponse>> call, Throwable t) {
        Toast.makeText(context, "Something went wrong!", Toast.LENGTH_LONG).show();
    }
}
