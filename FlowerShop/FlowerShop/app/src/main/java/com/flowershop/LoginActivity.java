package com.flowershop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.flowershop.api.controllers.LoginController;
import com.flowershop.api.models.AuthenticationDto;


public class LoginActivity extends AppCompatActivity {

    private EditText usernameField;
    private EditText passwordField;
    private LoginController loginController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginController = new LoginController(this);

        Button loginButton = findViewById(R.id.loginButton);
        Button registerButton = findViewById(R.id.registerButton);

        usernameField = findViewById(R.id.usernameLogin);
        passwordField = findViewById(R.id.passwordLogin);

        loginButton.setOnClickListener((view) -> loginButtonClick());
        registerButton.setOnClickListener((view) -> navigateToRegister());
    }

    private void loginButtonClick() {
        loginController.login(new AuthenticationDto(usernameField.getText().toString(), passwordField.getText().toString()));
    }

    private void navigateToRegister() {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));

        finish();
    }
}