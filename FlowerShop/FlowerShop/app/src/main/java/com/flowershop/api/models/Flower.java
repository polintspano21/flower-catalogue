package com.flowershop.api.models;

import com.flowershop.api.models.enums.FlowerCategory;

import kotlinx.coroutines.flow.Flow;

public class Flower {
    private Long id;
    private String name;
    private String description;
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private int price;
    private FlowerCategory category;

    public Flower() {}

    public Flower(Long id, String name, int price, FlowerCategory category) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public FlowerCategory getCategory() {
        return category;
    }

    public void setCategory(FlowerCategory category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
