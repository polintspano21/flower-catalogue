package com.flowershop;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.flowershop.api.models.Flower;

import java.util.ArrayList;

public class FlowerListAdapter extends RecyclerView.Adapter<FlowerListAdapter.FlowerHolder> {
    private ArrayList<Flower> flowerArrayList;
    private Context context;

    public FlowerListAdapter(ArrayList<Flower> flowers, Context context) {
        this.flowerArrayList = flowers;
        this.context = context;
    }

    @NonNull
    @Override
    public FlowerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item, parent, false);

        return new FlowerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FlowerHolder holder, int position) {
        Flower flower = flowerArrayList.get(position);

        holder.setFlowerName(flower.getName() + " $" + flower.getPrice());
        holder.setFlowerType(flower.getCategory().name());
        holder.loadImage(flower.getImageUrl());
        holder.setMoreInfoListener((view) -> {
            Intent flowerInfoIntent = new Intent(context, FlowerInfo.class);

            flowerInfoIntent.putExtra("flowerId", flower.getId());
            flowerInfoIntent.putExtra("flowerUrl", flower.getImageUrl());

            context.startActivity(flowerInfoIntent);
        });
    }

    @Override
    public int getItemCount() {
        return flowerArrayList == null? 0: flowerArrayList.size();
    }

    public class FlowerHolder extends RecyclerView.ViewHolder {
        private final ImageView imageView;
        private final TextView flowerName;
        private final TextView flowerType;
        private final Button moreInfo;

        public FlowerHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.flower_image);
            flowerName = itemView.findViewById(R.id.flower_name);
            flowerType = itemView.findViewById(R.id.flower_type);
            moreInfo = itemView.findViewById(R.id.more_info);
        }

        public void setMoreInfoListener(View.OnClickListener onClickListener) {
            moreInfo.setOnClickListener(onClickListener);
        }

        public void loadImage(String url) {
            new DownloadImageTask(imageView).execute(url);
        }

        public void setFlowerName(String name) {
            flowerName.setText(name);
        }

        public void setFlowerType(String type) {
            flowerType.setText(type);
        }
    }
}
