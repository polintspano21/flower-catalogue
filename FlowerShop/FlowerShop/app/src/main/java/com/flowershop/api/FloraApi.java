package com.flowershop.api;

import com.flowershop.api.models.AuthenticationDto;
import com.flowershop.api.models.AuthenticationRequestResponse;
import com.flowershop.api.models.Flower;
import com.flowershop.api.models.FlowerFilters;
import com.flowershop.api.models.RequestResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface FloraApi {

    @POST("/user/login")
    Call<RequestResponse<AuthenticationRequestResponse>> loginUser(@Body AuthenticationDto authenticationDto);

    @POST("/user/register")
    Call<RequestResponse<AuthenticationRequestResponse>> registerUser(@Body AuthenticationDto authenticationDto);

    @POST("/flowers/get")
    Call<RequestResponse<ArrayList<Flower>>> getFlowers(@Body FlowerFilters flowerFilters);

    @GET("/flower/get")
    Call<RequestResponse<Flower>> getFlower(@Query("flowerId") Long id);

    @DELETE("/flower/delete")
    Call<RequestResponse<Flower>> deleteFlower(@Query("flowerId") Long id);

    @POST("/flower/add")
    Call<RequestResponse<Flower>> addFlower(@Body Flower flower);

    @PUT("/flower/update")
    Call<RequestResponse<Flower>> updateFlower(@Body Flower flower);

}
