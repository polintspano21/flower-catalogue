package com.flowershop.api.models;

import androidx.annotation.NonNull;

import com.flowershop.api.models.enums.FlowerCategory;

import java.util.List;

public class FlowerFilters {
    private String name;
    private Integer minPrice;
    private Integer maxPrice;
    private List<FlowerCategory> categories;

    public FlowerFilters() { }

    public FlowerFilters(String name, Integer minPrice, Integer maxPrice, List<FlowerCategory> categories) {
        this.name = name;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.categories = categories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }

    public List<FlowerCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<FlowerCategory> categories) {
        this.categories = categories;
    }

    @NonNull
    @Override
    public String toString() {
        return name + " " + maxPrice + " " + minPrice + " " + categories;
    }
}
