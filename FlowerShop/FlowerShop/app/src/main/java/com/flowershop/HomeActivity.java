package com.flowershop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.flowershop.api.controllers.FlowerController;
import com.flowershop.api.models.Flower;
import com.flowershop.api.models.FlowerFilters;
import com.flowershop.api.models.enums.FlowerCategory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class HomeActivity extends AppCompatActivity {
    private FlowerListAdapter flowerListAdapter;
    private ArrayList<Flower> flowersList;
    private FlowerController flowerController;
    private List<FlowerCategory> flowerCategories;
    private EditText searchInput;
    private TextView filtersTextview;

    public HomeActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        filtersTextview = findViewById(R.id.filter_textview);
        searchInput = findViewById(R.id.flower_name_search);
        Button outdoorButton = findViewById(R.id.outdoor_filter);
        Button indoorButton = findViewById(R.id.indoor_filter);
        Button exoticButton = findViewById(R.id.exotic_filter);
        Button allButton = findViewById(R.id.all_filter);
        Button createFlower = findViewById(R.id.add_flower_home);
        Button searchFiler = findViewById(R.id.search_filter);

        flowerCategories = new ArrayList<>();
        flowersList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView flowerView = findViewById(R.id.flower_list_view);
        flowerView.setLayoutManager(layoutManager);
        flowerListAdapter = new FlowerListAdapter(flowersList, this);
        flowerController = new FlowerController(this);
        flowerView.setAdapter(flowerListAdapter);

        outdoorButton.setOnClickListener((view) -> toggleFilter(FlowerCategory.OUTDOOR));
        indoorButton.setOnClickListener((view) -> toggleFilter(FlowerCategory.INDOOR));
        exoticButton.setOnClickListener((view) -> toggleFilter(FlowerCategory.EXOTIC));
        searchFiler.setOnClickListener((view) -> searchButtonClick());
        allButton.setOnClickListener((view) -> allFlowersClick());
        createFlower.setOnClickListener((view) -> {
            Intent newIntent = new Intent(this, ManageFlower.class);
            newIntent.putExtra("mode", "create");

            startActivity(newIntent);
        });

        flowerController.loadFlowers(new FlowerFilters());
    }

    private void searchButtonClick() {
        FlowerFilters flowerFilters = new FlowerFilters();

        flowerFilters.setName(searchInput.getText().toString());
        if (flowerCategories.size() > 0) {
            flowerFilters.setCategories(flowerCategories);
        }

        flowerController.loadFlowers(flowerFilters);
    }

    private void updateFiltersTextView() {
        StringBuilder filtersText = new StringBuilder();

        filtersText.append("You are looking for ");
        flowerCategories.forEach((category) -> {
            filtersText.append(category.toString().toLowerCase());

            if (flowerCategories.size() >= 1) {
                filtersText.append(" ");
            }
        });

        if (flowerCategories.size() == 0) {
            filtersText.append("all ");
        }

        filtersText.append("flowers.");

        filtersTextview.setText(filtersText);
    }

    private void allFlowersClick() {
        flowerCategories.clear();

        FlowerFilters filters = new FlowerFilters();
        filters.setName(searchInput.getText().toString());

        flowerController.loadFlowers(filters);
    }

    private void toggleFilter(FlowerCategory category) {
        if (flowerCategories.contains(category)) {
            flowerCategories.remove(category);
        } else {
            flowerCategories.add(category);
        }

        FlowerFilters flowerFilters = new FlowerFilters();
        flowerFilters.setName(searchInput.getText().toString());

        flowerFilters.setCategories(flowerCategories);

        flowerController.loadFlowers(flowerFilters);
    }

    public void setFlowers(Iterable<Flower> flowers) {
        flowersList.clear();
        flowersList.addAll(StreamSupport.stream(flowers.spliterator(), false).collect(Collectors.toList()));
        updateFiltersTextView();
        flowerListAdapter.notifyDataSetChanged();
    }
}