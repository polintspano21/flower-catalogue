package com.flowershop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.flowershop.api.controllers.LoginController;
import com.flowershop.api.controllers.RegisterController;
import com.flowershop.api.models.AuthenticationDto;

public class RegisterActivity extends AppCompatActivity {

    private EditText usernameField;
    private EditText passwordField;
    private EditText passwordConfirmField;
    private LoginController loginController;
    private RegisterController registerController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        this.registerController = new RegisterController(this);

        Button loginButton = findViewById(R.id.go_to_login_activity_button);
        Button registerButton = findViewById(R.id.register_button);

        usernameField = findViewById(R.id.register_username);
        passwordField = findViewById(R.id.register_password);
        passwordConfirmField = findViewById(R.id.register_password_confirm);

        loginButton.setOnClickListener((view) -> navigateToRegister());
        registerButton.setOnClickListener((view) -> registerButtonClick());
    }

    private void registerButtonClick() {
        if (passwordConfirmField.getText().toString().equals(passwordField.getText().toString())) {
            registerController.register(new AuthenticationDto(usernameField.getText().toString(), passwordConfirmField.getText().toString()));
        } else {
            Toast.makeText(this, "Passwords must match!", Toast.LENGTH_LONG).show();
        }

    }

    private void navigateToRegister() {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));

        finish();
    }
}