package com.flowershop.api.controllers;

import android.content.Context;

import com.flowershop.HomeActivity;
import com.flowershop.api.FloraApi;
import com.flowershop.api.models.Flower;
import com.flowershop.api.models.FlowerFilters;
import com.flowershop.api.models.RequestResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FlowerController implements Callback<RequestResponse<ArrayList<Flower>>> {
    private final String BASE_URL = "http://192.168.0.107:8080";
    private final Context context;

    public FlowerController(Context context) {
        this.context = context;
    }

    public void loadFlowers(FlowerFilters flowerFilters) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        FloraApi gerritAPI = retrofit.create(FloraApi.class);

        Call<RequestResponse<ArrayList<Flower>>> call = gerritAPI.getFlowers(flowerFilters);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<RequestResponse<ArrayList<Flower>>> call, Response<RequestResponse<ArrayList<Flower>>> requestResponse) {
        ((HomeActivity) context).setFlowers(requestResponse.body().getEntity());
    }

    @Override
    public void onFailure(Call<RequestResponse<ArrayList<Flower>>> call, Throwable t) {
        t.printStackTrace();
    }
}
