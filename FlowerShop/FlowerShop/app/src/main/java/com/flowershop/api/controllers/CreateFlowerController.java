package com.flowershop.api.controllers;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.flowershop.HomeActivity;
import com.flowershop.api.FloraApi;
import com.flowershop.api.models.Flower;
import com.flowershop.api.models.RequestResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateFlowerController implements Callback<RequestResponse<Flower>> {
    private final String BASE_URL = "http://192.168.0.107:8080";
    private final Context context;

    public CreateFlowerController(Context context) {
        this.context = context;
    }

    public void createFlower(Flower fLower) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        FloraApi gerritAPI = retrofit.create(FloraApi.class);

        Call<RequestResponse<Flower>> call = gerritAPI.addFlower(fLower);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<RequestResponse<Flower>> call, Response<RequestResponse<Flower>> response) {
        if (response.code() == 200) {
            context.startActivity(new Intent(context, HomeActivity.class));
        } else {
            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call<RequestResponse<Flower>> call, Throwable t) {

    }
}
