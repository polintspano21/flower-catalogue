package com.flowershop;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.flowershop.api.controllers.CreateFlowerController;
import com.flowershop.api.controllers.UpdateFlowerController;
import com.flowershop.api.models.Flower;
import com.flowershop.api.models.enums.FlowerCategory;

import java.util.Objects;

public class ManageFlower extends AppCompatActivity {
    private ImageView imageView;
    private EditText flowerNameEdit;
    private EditText flowerPriceEdit;
    private EditText flowerCategoryEdit;
    private EditText flowerDescriptionEdit;
    private EditText flowerImageUrl;
    private CreateFlowerController createFlowerController;
    private UpdateFlowerController updateFlowerController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_flower);

        flowerNameEdit = findViewById(R.id.cu_flower_name);
        flowerPriceEdit = findViewById(R.id.cu_flower_price);
        flowerDescriptionEdit = findViewById(R.id.cu_flower_description);
        flowerCategoryEdit = findViewById(R.id.cu_flower_category);
        flowerImageUrl = findViewById(R.id.cu_flower_image_url);
        Button saveButton = findViewById(R.id.save_flower);
        Button createButton = findViewById(R.id.add_flower);
        imageView = findViewById(R.id.imageView3);
        Bundle extras = getIntent().getExtras();

        createFlowerController = new CreateFlowerController(this);
        updateFlowerController= new UpdateFlowerController(this);

        createButton.setOnClickListener((view) -> addFlowerClick());

        if (Objects.equals(extras.getString("mode"), "edit")) {
            saveButton.setOnClickListener((view) -> updateFlowerClick(extras.getLong("flowerId")));
            flowerNameEdit.setText(extras.getString("flowerName"));
            flowerPriceEdit.setText(String.valueOf(extras.getInt("flowerPrice")));
            flowerCategoryEdit.setText(extras.getString("flowerCategory"));
            flowerDescriptionEdit.setText(extras.getString("flowerDescription"));
            flowerImageUrl.setText(extras.getString("flowerUrl"));
            new DownloadImageTask(imageView).execute(extras.getString("flowerUrl"));


            createButton.setVisibility(View.GONE);
        } else {
            saveButton.setVisibility(View.GONE);
        }
    }

    public void addFlowerClick() {
        Flower newFlower = new Flower();
        System.out.println(flowerDescriptionEdit.getText().toString());
        newFlower.setDescription(flowerDescriptionEdit.getText().toString());
        newFlower.setName(flowerNameEdit.getText().toString());
        newFlower.setCategory(FlowerCategory.valueOf(flowerCategoryEdit.getText().toString()));
        newFlower.setPrice(Integer.parseInt(flowerPriceEdit.getText().toString()));
        newFlower.setImageUrl(flowerImageUrl.getText().toString());
        createFlowerController.createFlower(newFlower);
    }

    public void updateFlowerClick(Long id) {
        Flower newFlower = new Flower();
        newFlower.setId(id);
        newFlower.setDescription(flowerDescriptionEdit.getText().toString());
        newFlower.setName(flowerNameEdit.getText().toString());
        newFlower.setCategory(FlowerCategory.valueOf(flowerCategoryEdit.getText().toString()));
        newFlower.setPrice(Integer.parseInt(flowerPriceEdit.getText().toString()));
        newFlower.setImageUrl(flowerImageUrl.getText().toString());
        updateFlowerController.updateFlower(newFlower);
    }
}