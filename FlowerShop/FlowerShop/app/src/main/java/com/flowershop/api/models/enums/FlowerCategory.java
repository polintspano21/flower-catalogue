package com.flowershop.api.models.enums;

public enum FlowerCategory {
    INDOOR,
    OUTDOOR,
    EXOTIC
}

