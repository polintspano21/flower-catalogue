package com.flowershop.api.models.enums;

public enum UserRole {
    ADMIN,
    USER
}
