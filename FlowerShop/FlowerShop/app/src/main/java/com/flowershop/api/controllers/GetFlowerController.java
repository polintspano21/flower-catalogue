package com.flowershop.api.controllers;

import android.content.Context;

import com.flowershop.FlowerInfo;
import com.flowershop.api.FloraApi;
import com.flowershop.api.models.Flower;
import com.flowershop.api.models.RequestResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GetFlowerController implements Callback<RequestResponse<Flower>> {
    private final String BASE_URL = "http://192.168.0.107:8080";
    private final Context context;

    public GetFlowerController(Context context) {
        this.context = context;
    }

    public void getFlowerById(Long id) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        FloraApi gerritAPI = retrofit.create(FloraApi.class);

        Call<RequestResponse<Flower>> call = gerritAPI.getFlower(id);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<RequestResponse<Flower>> call, Response<RequestResponse<Flower>> response) {
        ((FlowerInfo) context).setFlowerInfo(response.body().getEntity());
    }

    @Override
    public void onFailure(Call<RequestResponse<Flower>> call, Throwable t) {

    }
}
