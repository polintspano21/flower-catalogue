package com.flowershop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.flowershop.api.controllers.DeleteFlowerController;
import com.flowershop.api.controllers.GetFlowerController;
import com.flowershop.api.models.Flower;

public class FlowerInfo extends AppCompatActivity {
    private Flower flower;
    private ImageView flowerImage;
    private TextView titleTextView;
    private TextView flowerDescription;
    private GetFlowerController flowerController;
    private DeleteFlowerController deleteFlowerController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flower_info);

        flowerImage = findViewById(R.id.flower_info_image);
        titleTextView = findViewById(R.id.flower_info_title);
        flowerDescription = findViewById(R.id.flower_info_description);
        Button deleteFlower = findViewById(R.id.delete_flower);
        Button editFlower = findViewById(R.id.edit_flower);

        Bundle extras = getIntent().getExtras();

        flowerController = new GetFlowerController(this);
        deleteFlowerController = new DeleteFlowerController(this);
        flowerController.getFlowerById(extras.getLong("flowerId"));

        new DownloadImageTask(flowerImage).execute(extras.getString("flowerUrl"));

        deleteFlower.setOnClickListener((view) -> deleteFlowerController.deleteFlower(extras.getLong("flowerId")));
        editFlower.setOnClickListener((view) -> {
            Intent newIntent = new Intent(this, ManageFlower.class);
            newIntent.putExtra("mode", "edit");
            newIntent.putExtra("flowerId", flower.getId());
            newIntent.putExtra("flowerName", flower.getName());
            newIntent.putExtra("flowerPrice", flower.getPrice());
            newIntent.putExtra("flowerUrl", flower.getImageUrl());
            newIntent.putExtra("flowerDescription", flower.getDescription());
            newIntent.putExtra("flowerCategory", flower.getCategory().name());

            startActivity(newIntent);
        });
    }

    public void setFlowerInfo(Flower flower) {
        this.flower = flower;
        titleTextView.setText(new StringBuilder().append(flower.getName()).append(", ").append(flower.getCategory().name().toLowerCase()).append(", $").append(flower.getPrice()));
        flowerDescription.setText(flower.getDescription() == null ? "No description!" : flower.getDescription());

        if (flower.getDescription() == null) {
            flowerDescription.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
    }
}